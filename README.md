# Data Visualization Application with Autodesk Forge
Copyright (c) 2018 Rohan Vora

Author: Rohan Vora [rovora@pdx.edu]



## Information about Forge

The [autodesk forge](https://forge.autodesk.com/) platform allows Autodesk CAD users to access their data and build customizable solutions in the cloud.

The Forge platform contains several APIs for third party web development. This application just uses the OAuth and Data Management APIs.

## Using this Application

In order to use this application you will need an Autodesk account. Anyone with a .edu email can create one for free at the Autodesk site.

Or, if you want to try it out, shoot me and email and I can send you my Autodesk login.

Without the login you can still boot up the server, and the static file will be served, but you won't be able to make use of the functionality.


## Data Visualization

The idea is to use the applicication to do a useful data visualization of a users Autodesk data using D3.js. The D3 functionallity has been 
written and lives in the `main.html` file, but is currently turned off / slightly buggy.

The target user's data is a network of part and object files contained inside a Fusion 360 CAD file. The traditional file explorer provided by Autodesk is a hierarchical file explorer.
This is inadequate when the the data is a graph rather than a tree. This application attempts to provide a interactive visualization of the data in a way that better represents
the project structure. 

For the scope of the RUST 410 class I'm going to focus on implementing the server side stuff since that's the stuff in Rust.

## Application Structure

The codebase for this application is super simple and monolithic right now. It will eventually be structured differently.

As it stands the server code lives in the `main.rs` file and the static webpage is in the `main.html` file.

When the server is booted it up it serves the static page. From there, users authetnicate via a 3-legged authentication with the Autodesk Forge OAuth API.

Once an authorization code has been obtained, the user can navigate to their data using the interface on the webpage. The page makes a POST request to the server, which in turn pings
the Autodesk DM API for the data, and then serves it up to the webpage. 

The actual data visualization has been turned off for now since I wrote it a while ago and the D3 stuff is deprecrated. 


