// Rohan Vora
// CS410 RUst
// Project

//Project Description: The purpose of this project is to build a application and web server in Rust using the Rocket framework.
//The function of the server is to initate calls to API endpoints at the on the Autodesk Forge platform.
// The server initialy serves a static file. The, incoming POSTS from the web browsers are handled,
// and the appropriate POST query pointing at the Autodesk Forge API endpoints is constructed.
// The response from this inner POST is then sent back to the browser, where the JS stuff handles display.


#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

//Dependencies and namespaces 
extern crate reqwest;
extern crate curl;
extern crate rocket;
extern crate rustc_serialize;
use rustc_serialize::json::Json;
use std::collections::HashMap;
use std::io::{stdout, Write};
use std::path::{Path, PathBuf};
use rocket::response::NamedFile;
use rocket::http::{Cookie, Cookies};
use curl::easy::Easy;
use reqwest::header::{Headers, Authorization, Bearer};
// use rocket::http::hyper::header::{Headers, Authorization, Bearer};


//Handler for a generic request to the root.
#[get("/")]
fn index() -> std::io::Result<NamedFile> {
    NamedFile::open("public/main.html")
    
}

//Data forms for incoming requests to the server.
// Could be condensed?
#[derive(FromForm)]
struct Token{
   code: String,
}
#[derive(FromForm)]
struct Id{
    id: String,
}

#[derive(FromForm)]
struct Hp_Table{
    hub_id: String,
    proj_id: String,
}

#[derive(FromForm)]
struct Fp_Table{
    proj_id: String,
    folder_id: String,
}

#[derive(FromForm)]
struct Tip_Table{
    item_id: String,
    proj_id: String,
}



//Handler for parsing the URL encoded access token from the Autodesk login.
#[get("/?<token>")]
fn get_token(token: Token, mut cookies: Cookies ) -> std::io::Result<NamedFile>{


    //Confirm correct code has been parsed.
    println!("{}", token.code);


    //Build post request for getting authorization code.
    let client = reqwest::Client::new();
    let mut params = HashMap::new();
    params.insert("client_id","lid8BC5A7GweMbtp5TauLFHEIpx7vmK9");
    params.insert("client_secret", "F86db7542d8e1486");
    params.insert("grant_type", "authorization_code");
    params.insert("code", &token.code);
    params.insert("redirect_uri", "http://localhost:8000");

    let res = client.post("https://developer.api.autodesk.com/authentication/v1/gettoken")
        .form(&params)
        .send();

     let jtxt = match res {
        Ok(mut t) =>  t.text().unwrap(),
        Err(t) =>  "error".to_string()
    };

    let json = Json::from_str(&jtxt).unwrap();


    //Parsing response and storing in cookies. (This could be cleaner.)
    let jsonq = json.find_path(&["access_token"]).unwrap().to_string();
    let access_code = jsonq.replace("\"","");
    println!("{}", access_code);
    cookies.add(Cookie::new("User", access_code));

    NamedFile::open("public/main.html")

}
// Handler for a get_hubs post to the root. Returns JSON text.
#[post("/")]
fn get_hubs(cookies: Cookies) -> String {

  let mac = cookies.get("User").map(|c|  c.value()).unwrap();

  let mut headers = Headers::new();
  headers.set(
      Authorization(
          Bearer {
              token: mac.to_owned()
          }
      )
  );

  let client = reqwest::Client::builder()
        .default_headers(headers)
        .build().unwrap();
  let res = client.get("https://developer.api.autodesk.com/project/v1/hubs").send();
  
  
  let jtxt = match res {
        Ok(mut t) =>  t.text().unwrap(),
        Err(_t) =>  "error".to_string()
    };


    jtxt



 // NamedFile::open("public/main.html")
    
}

// Handler for a get_projects request. Returns JSON text.
#[post("/get_projects?<id>")]
fn get_projects(id: Id, cookies: Cookies) -> String{


   let mac = cookies.get("User").map(|c|  c.value()).unwrap();

   let mut headers = Headers::new();
   headers.set(
      Authorization(
          Bearer {
              token: mac.to_owned()
          }
      )
  );

  let api_endpoint: String = "https://developer.api.autodesk.com/project/v1/hubs/".to_string();
  let full = format!("{}{}{}", api_endpoint, id.id, "/projects");
  println!("{}", full);

  

  let client = reqwest::Client::builder()
        .default_headers(headers)
        .build().unwrap();
  let res = client.get(&full).send();
  
  
  let jtxt = match res {
        Ok(mut t) =>  t.text().unwrap(),
        Err(_t) =>  "error".to_string()
    };

    println!("{}", jtxt);

    jtxt 

    

}

// Handler for the get_project_root request. Returns JSON text.
#[post("/get_project_root?<hp_table>")]
fn get_project_root(hp_table: Hp_Table, cookies: Cookies) -> String{
    println!("{}{}", hp_table.hub_id, hp_table.proj_id);

    let mac = cookies.get("User").map(|c|  c.value()).unwrap();

   let mut headers = Headers::new();
   headers.set(
      Authorization(
          Bearer {
              token: mac.to_owned()
          }
      )
  );

  let api_endpoint: String = "https://developer.api.autodesk.com/project/v1/hubs/".to_string();
  let full = format!("{}{}{}{}{}", api_endpoint, hp_table.hub_id, "/projects/", hp_table.proj_id, "/topFolders");
  println!("{}", full);

  

  let client = reqwest::Client::builder()
        .default_headers(headers)
        .build().unwrap();
  let res = client.get(&full).send();
  
  
  let jtxt = match res {
        Ok(mut t) =>  t.text().unwrap(),
        Err(_t) =>  "error".to_string()
    };

    println!("{}", jtxt);

    jtxt 

}

//Handler for the get_porject root request. Returns JSON text.
#[post("/get_folder_contents?<fp_table>")]
fn get_folder_contents(fp_table: Fp_Table, cookies: Cookies) -> String{
    println!("{}{}", fp_table.proj_id, fp_table.folder_id);

    let mac = cookies.get("User").map(|c|  c.value()).unwrap();

   let mut headers = Headers::new();
   headers.set(
      Authorization(
          Bearer {
              token: mac.to_owned()
          }
      )
  );

  let api_endpoint: String = "https://developer.api.autodesk.com/data/v1/projects/".to_string();
  let full = format!("{}{}{}{}{}", api_endpoint, fp_table.proj_id, "/folders/", fp_table.folder_id, "/contents");
  println!("{}", full);

  

  let client = reqwest::Client::builder()
        .default_headers(headers)
        .build().unwrap();
  let res = client.get(&full).send();
  
  
  let jtxt = match res {
        Ok(mut t) =>  t.text().unwrap(),
        Err(_t) =>  "error".to_string()
    };

    println!("{}", jtxt);

    jtxt 

}

//Handler for getting tip version of Project data
#[post("/get_tip?<tip_table>")]
fn get_tip(tip_table: Tip_Table, cookies: Cookies) -> String{
    println!("{}{}", tip_table.proj_id, tip_table.item_id);

    let mac = cookies.get("User").map(|c|  c.value()).unwrap();

   let mut headers = Headers::new();
   headers.set(
      Authorization(
          Bearer {
              token: mac.to_owned()
          }
      )
  );

  let api_endpoint: String = "https://developer.api.autodesk.com/data/v1/projects/".to_string();
  let full = format!("{}{}{}{}{}", api_endpoint, tip_table.proj_id, "/items/", tip_table.item_id, "/tip");
  println!("{}", full);

  

  let client = reqwest::Client::builder()
        .default_headers(headers)
        .build().unwrap();
  let res = client.get(&full).send();
  
  
  let jtxt = match res {
        Ok(mut t) =>  t.text().unwrap(),
        Err(_t) =>  "error".to_string()
    };

    println!("{}", jtxt);

    jtxt 

}

//Creates an instance of the server.
//I'm not certain about what the standard is here, but I have a feeling this is too many mounted routes.
//A better implementation could be to group routes into two or three groups, and then do the branching logic
// Inside of the those 3 high level handlers.
fn main() {
    rocket::ignite().mount("/", routes![index,get_token,get_hubs,get_projects, get_project_root, get_folder_contents, get_tip]).launch();
    
    
}


